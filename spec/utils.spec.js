
fs = require('fs')
myCode = fs.readFileSync('./utils.js','utf-8') // depends on the file encoding
eval(myCode)

describe('utils fixDateArray', function(){


    it('should be able to create a date object from twitterAPI string date and replace the the fieldToFix in the array',function(){

        var data = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var expectedData = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var fieldToFix = "created_at";
        expect(Object.prototype.toString.call(fixDateArray(data,fieldToFix)[0][fieldToFix])).toBe("[object Date]");
    });


    it('should throw error when undefined fieldToFix',function(){

        var data = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var expectedData = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var fieldToFix;
        expect( function(){ fixDateArray(data,fieldToFix); } ).toThrow(new Error("Data is not an array or is not Defined or fieldToFix is Undefined"));
    });

    
    it('should throw error when undefined data',function(){

        var data;
        var expectedData = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var fieldToFix = "created_at";
        expect( function(){ fixDateArray(data,fieldToFix); } ).toThrow(new Error("Data is not an array or is not Defined or fieldToFix is Undefined"));
    });

    it('should throw error when data is not an array',function(){

        var data = "Thu Jun 10 22:46:57 +0000 2010";
        var expectedData = [{created_at: "Thu Jun 10 22:46:57 +0000 2010"}];
        var fieldToFix = "created_at";
        expect( function(){ fixDateArray(data,fieldToFix); } ).toThrow(new Error("Data is not an array or is not Defined or fieldToFix is Undefined"));
    });


});