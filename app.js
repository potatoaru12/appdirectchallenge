(function(){
    var app = angular.module("appDirectChallenge",["ngRoute","ngStorage"]);
    app.config(function($routeProvider){
      $routeProvider
        .when("/main",{
          templateUrl: "twitter.html",
          controller: "TwitterController"
        })
        .otherwise({redirectTo:"/main"});
    });
    
  }());