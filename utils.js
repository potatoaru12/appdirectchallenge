var fixDateArray = function (data, fieldToFix) {

	if (Array.isArray(data) && data != undefined && fieldToFix != undefined) {
		data.forEach(function (key) {
			var newDate = new Date(key.created_at);
			key[fieldToFix] = newDate;
		});
	} else {
		throw new Error("Data is not an array or is not Defined or fieldToFix is Undefined");
	}
	return data;
};