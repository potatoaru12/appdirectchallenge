# appdirect-code-challenge-solution

small web app built on twitter rest api

## Setup

Install the project dependencies:

`npm install`

## Running

Starts the static and proxy servers:

`npm start`

