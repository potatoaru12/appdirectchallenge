(function(){
  
    var twitterService = function($http){
      
      var getUser = function(username,tweetCount){
        return $http.get("http://localhost:7890/1.1/statuses/user_timeline.json?count="+tweetCount+"&screen_name=" + username)
                    .then(function(response){
                      return response.data;
                    });
      };
      

      var getRetweets = function(retweetId){
        return $http.get("http://localhost:7890/1.1/statuses/retweets/" + retweetId+".json")
                    .then(function(response){
                      return response.data;
                    });
      };
      
      return {
        getUser: getUser,
        getRetweets: getRetweets
      };
    };
    
    var module = angular.module("appDirectChallenge");
    module.factory("twitterService",twitterService);
  }());