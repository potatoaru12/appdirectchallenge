(function () {

  var app = angular.module("appDirectChallenge");


  var TwitterController = function ($scope, twitterService, $log, $routeParams, $localStorage, $window) {

    var fieldToFix = "created_at";

    //process successful user tweets data from promise
    var onUserComplete = function (data) {
      $scope.tweets = fixDateArray(data, fieldToFix);
    };

    var onUser2Complete = function (data) {
      $scope.tweets2 = fixDateArray(data, fieldToFix);
    };

    var onUser3Complete = function (data) {
      $scope.tweets3 = fixDateArray(data, fieldToFix);
    };

    //process successful retweets data from promise
    var onRetweetsComplete = function (data) {
      $scope.retweets = data;
      retweetModal.openModal();
      if (!Array.isArray(data) || !data.length) {
        $scope.retweetMessage = "Unfortunately, Twitter has returned no user retweets despite indicating a non zero number. Please try another one.";
      }
      else {
        $scope.retweetMessage = "";
      }

    };

    //process on error from promise
    var onError = function (reason) {
      $scope.displayMessage = "Could not fetch the user while trying to fetch twitter data " + JSON.stringify(reason);
    };

    // get retweets from service
    $scope.search = function (tweetid) {
      twitterService.getRetweets(tweetid).then(onRetweetsComplete, onError);
    };

    //fill tables with data from service by calling get user tweets from service
    $scope.loadDataInTables = function () {
      $scope.displayMessage = "OK";
      for (var i = 0; i < $scope.$storage.layoutSettings.tableUser.length; i++) {
        twitterService.getUser($scope.$storage.layoutSettings.tableUser[i], $scope.$storage.layoutSettings.numberOfTweets).then(onComplete[i], onError);
      }
    }

    $scope.clearStorage = function () {
      $window.localStorage.clear();
      $scope.displayMessage = "Local Storage has been cleared. Refresh the page to see the default settings being applied again."
    }

    //Local storage default
    $scope.$storage = $localStorage.$default({
      layoutSettings: {
        tableUser: ["appdirect", "LaughingSquid", "TechCrunch"],
        numberOfTweets: 30
      }
    });

    var onComplete = [onUserComplete, onUser2Complete, onUser3Complete];
    $scope.loadDataInTables();

  };

  app.controller("TwitterController", TwitterController);



}());